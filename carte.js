// class repas contenant le nom du repas, l'entrée, le plat principal et dessert
class repas {
    constructeur(nomRepas, entree, principal, dessert) {
        this.nomRepas = nomRepas;
        this.entree = entree;
        this.principal = principal;
        this.dessert = dessert;
    }
}

// tableau des menu contenant les divers repas proposés
const menu = [];

// fonction permettant à l'utilisateur de rentrer dans la console les repas
function getRepas () {

    // demande une question sur les repas (nom du repas/entrée/plat principal/dessert) 
    const readline = require('readline')
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })
    

    const repas1 = new repas(); 


    const questionNomRepas = () => {
        return new Promise((resolve, reject) => {
            rl.question('Saisir le nom du repas : ', (nomRepas) => {
            resolve(repas1.nomRepas = nomRepas);
            })
        })
    }

    const questionEntree = () => {
        return new Promise((resolve, reject) => {
            rl.question("Saisir l'entrée : ", (entree) => {
            resolve(repas1.entree = entree);
            })
        })
    }

    const questionPrincipal = () => {
        return new Promise((resolve, reject) => {
            rl.question('Saisir le principal : ', (principal) => {
            resolve(repas1.principal = principal);
            })
        })
    }

    const questionDessert = () => {
        return new Promise((resolve, reject) => {
            rl.question('Saisir le dessert : ', (dessert) => {
            resolve(repas1.dessert = dessert);

            })
        })
    }

    const main = async () => {
        await questionNomRepas();
        await questionEntree();
        await questionPrincipal();
        await questionDessert();

        rl.close();  
        
        // message confirmation
        ajouterRepas(repas1);
    }
    main();
    
}

// fonction pour ajouter un repas
function ajouterRepas (repas) {
    const readline = require('readline')
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    let reponse;

    const questionConfirmer = () => {
        return new Promise((resolve, reject) => {
            rl.question("Confirmer l'ajout du repas dans le menu ? [y/n] : ", (r) => {
            resolve(reponse = r);
            

            })
        })
    }

    const main = async () => {
        await questionConfirmer();
        rl.close(); 
        
        if (reponse === 'y') {
            menu.push(repas);
            continuerSaisieRepas();     
        }
        else if (reponse === 'n') {
            continuerSaisieRepas();
        }
        else {
            console.log("ERROR");
        }
    }
    main();
}

// fonction pour demander de continuer la saisie
// demande une question sur la carte (continue de saisir/afficher) 
function continuerSaisieRepas () {
    const readline = require('readline')
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    let reponse;

    const questionConfirmer = () => {
        return new Promise((resolve, reject) => {
            rl.question("Voulez-vous continuer la saisie ou afficher la carte ? [c/a] : ", (r) => {
            resolve(reponse = r);
            })
        })
    }

    const main = async () => {
        await questionConfirmer();
        
        rl.close();
        // condition pour continuer la saisie ou afficher la carte
        if (reponse === 'c') {
            getRepas();
        }
        else if (reponse === 'a') {
            afficherCarte();
        }
        else {
            console.log("ERREUR !");
        }        
    }
    main();
}

// fonction pour afficher la carte
function afficherCarte () {
    const tailleMax = definirTailleCarte();
    
    let ligne = "";

    // affiche #
    for (let i=0; i < tailleMax; i++) {
        ligne = ligne + "#";
    }
    console.log(ligne);
    
    // affiche LA CARTE #
    ligne = "# ";
    for (let i=0; i < tailleMax/2 - 6; i++) { // "-4 enlève la moitié de la carte -2 le début du template"
        ligne = ligne + " ";
    }
    ligne = ligne + "LA CARTE";
    for (let i=0; i < tailleMax/2 - 6; i++) { 
        ligne = ligne + " ";
    }
    if (tailleMax % 2 === 0) {
        ligne = ligne + " #";
    }
    else {
        ligne = ligne + "#";
    }
    console.log(ligne);

    // affiche #-#
    ligne = "#";
    for (let i=0; i < tailleMax - 2; i++) {
        ligne = ligne + "-";
    }
    ligne = ligne + "#";
    console.log(ligne);

    // affiche le corps
    for (let i=0; i < menu.length; i++) {
        // affiche les #-#
        ligne = "#";
        for (let j=0; j < tailleMax - 2; j++) {
            ligne = ligne + "-";
        }
        ligne = ligne + "#";
        console.log(ligne);

        // affiche nom repas
        ligne = "# " + menu[i].nomRepas;
        for (let j=0; j < tailleMax - menu[i].nomRepas.length - 4; j++) {
            ligne = ligne + " ";
        }
        ligne = ligne + " #";
        console.log(ligne);
        
        // affiche entree
        ligne = "#- " + menu[i].entree;
        for (let j=0; j < tailleMax - menu[i].entree.length - 5; j++) {
            ligne = ligne + " ";
        }
        ligne = ligne + " #";
        console.log(ligne);

        // affiche principal
        ligne = "#- " + menu[i].principal;
        for (let j=0; j < tailleMax - menu[i].principal.length - 5; j++) {
            ligne = ligne + " ";
        }
        ligne = ligne + " #";
        console.log(ligne);

        // affiche dessert
        ligne = "#- " + menu[i].dessert;
        for (let j=0; j < tailleMax - menu[i].dessert.length - 5; j++) {
            ligne = ligne + " ";
        }
        ligne = ligne + " #";
        console.log(ligne);
    }

    // affiche #
    ligne = "";
    for (let i=0; i < tailleMax; i++) {
        ligne = ligne + "#";
    }
    console.log(ligne);
}

// fonction pour définir la largeur max de la carte
function definirTailleCarte() {
    let tailleMax = 12; // longueur de la ligne LA CARTE (longueur mini)

    for (let i=0; i < menu.length; i++) {
        if (menu[i].nomRepas.length + 4 > tailleMax) { // + 4 pour les caractères en plus pour mettre en former la ligne
            tailleMax = menu[i].nomRepas.length + 4;
            console.log(tailleMax);
        }
        if (menu[i].entree.length + 5 > tailleMax ) { // + 5 idem
            tailleMax = menu[i].entree.length + 5;
        }
        if (menu[i].principal.length + 5 > tailleMax ) {
            tailleMax = menu[i].principal.length + 5;
        }
        if (menu[i].dessert.length + 5 > tailleMax ) {
            tailleMax = menu[i].dessert.length + 5;
        }  
    }
    return tailleMax;
}


// demande une question sur la carte (créer/afficher/quitter) 
const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

let quitter;

const questionQuitter = () => {
    return new Promise((resolve, reject) => {
        rl.question("Voulez-vous créer une carte ou l'afficher ou quitter  ? [c/a/q] : ", (q) => {
        resolve(quitter = q);
        })
    })
}

const main = async () => {
    await questionQuitter();

    rl.close();  
    
    // condition pour créer, afficher la carte ou la quitter
    if(quitter === 'c') {
        getRepas();
    }
    else if (quitter === 'a') {
        afficherCarte();
    }
    else if (quitter === 'q') {
        console.log("Vous avez quitté");
    }
    else {
        console.log("ERREUR !");
    }     
}
main();
